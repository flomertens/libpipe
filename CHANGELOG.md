# Change Log

## 0.4 - 2024-07-09

- [Changed] Add keepalive and allow agent forwarding by default

## 0.3 - 2023-04-14

- [New] Add task_done_callback() which is a callback function which take the task object as argument.

## 0.2 - 2023-02-12

- [Changed] worker: run all command with bash -c
- [Changed] nodetool: by default, if no nodes are set in the command line, run_joblist will use the nodes from the joblist file

## 0.1.7.2 - 2022-11-28

- [Changed] Update dependencies.

## 0.1.7.1 - 2022-07-22

- [Changed] Update dependencies.

## 0.1.7 - 2022-01-28

- [New] add nodetool add_job and nodetool run_joblist to replace the libnodes_bash.sh script providing finer control.
- [Fixed] worker: start command with bash to make sure we can pass all the sourced env variables to the command.

## 0.1.6.1  - 2021-10-08

- [Fixed] fix compatibility to Python 3.6

## 0.1.6  - 2021-10-07

- [New] worker: better handling of SIGTERM and SIGINT. Local and remote process are cleanly terminated in case of SIGTERM/SIGINT.
- [New] worker: implement pid_filename, a file which store the host and pid of the process when started ad that is removed after the process finished, unless keep_pid_file_on_error is True and the process does not stop cleanly.

## 0.1.5  - 2021-04-30

- [Removed] libpipe.msutils has been moved to nenucal.msutils. Please install [nenucal](https://gitlab.com/flomertens/nenucal-cd) if you want to keep this functionality.
