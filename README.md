Libpipe
=======

Common pipeline framework and library

Installation
------------

libpipe can be installed via pip:

    $ pip install libpipe

and requires Python 3.6.0 or higher.
