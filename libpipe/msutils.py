#
# libpipe.msutils has been moved to nenucal.msutils
#

raise DeprecationWarning('The libpipe.msutils module is deprecated and replaced by nenucal.msutils'
              'Please install nenucal-cd: https://gitlab.com/flomertens/nenucal-cd')
